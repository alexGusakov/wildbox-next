const withSass = require("@zeit/next-sass");
const withImages = require('next-images');
const withCSS = require('@zeit/next-css');

module.exports = withCSS(withSass(withImages()));

//module.exports = withSass();
