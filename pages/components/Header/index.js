import React from "react";

import LinkPure from "../LinkPure"

import wildboxLogo from "../../img/other/wildbox-logo.svg";
import wildboxText from "../../img/other/wildbox-text.svg";

import classes from "./index.module.scss";

const Header = props => (
	<div className={classes.root}>
		<LinkPure to="/" className={classes.logoWrapper}>
			<img alt="" className={classes.logo} src={wildboxLogo} />
			<img alt="" className={classes.text} src={wildboxText} />
		</LinkPure>
		<div className={classes.about}>
			<button onClick={props.opendDialog.open} className={classes.button}>О нашей платформе</button>
		</div>
	</div>
)

export default Header;