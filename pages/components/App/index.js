import React, {Fragment, Component} from "react";
import Helmet from "react-helmet";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import classnames from "classnames";
import { Container, Row, Col } from "react-grid-system";
import { Table } from "antd";
import { Divider } from "antd";
import { AutoComplete } from "antd";
import { Select } from "antd";
import { Spin } from "antd";
import { Breadcrumb } from "antd";
import { Rate } from "antd";
import { AreaChart, Area, defs, linearGradient, XAxis, YAxis, stop, Tooltip, ResponsiveContainer, CartesianGrid, Line, LineChart } from "recharts";
import request from "../../js/request";
import debounce from "debounce";
 
import Header from "../Header"
import Space from "../Space"
import Footer from "../Footer"
import LinkPure from "../LinkPure"
import FragmentLifecycle from "../FragmentLifecycle"
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Slide from "@material-ui/core/Slide";

//import favicon32x32 from "../../img/logo/favicon-32x32.png";
//import favicon16x16 from "../../img/logo/favicon-16x16.png";
//import safariPinnedTab from "../../img/logo/safari-pinned-tab.svg";

import classes from "./index.module.scss";
import mixins from "../../css/mixins.module.scss";

const { Option } = Select

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dialogIsOpen: false,

			currentBrand: null,

			foundBrands: [],

			mainTableData: [],
			mainTableRawData: [],
			mainTablePerPage: 25,
			mainTablePage: 1,
			mainTableDays: 7,
			mainTableTotal: 0,
			mainTableIsLoading: true,

			salesGraphIsLoading: false,
			salesGraphData: [],
			salesGraphDays: 7,

			categoriesTableData: [],
			categoriesTableRawData: [],
			categoriesTablePerPage: 3,
			categoriesTableDays: 7,
			categoriesTableIsLoading: true,

			productsTableData: [],
			productsTableRawData: [],
			productsTablePerPage: 4,
			productsTableDays: 7,
			productsTableIsLoading: true,
			productsTableTotal: 0,
			productsTablePage: 1,

			isMobile: false,
		};

		this.handlers = {
			getDateRange: days => {
				let now = new Date()
				return {
					to: this.handlers.dateToTimestamp(new Date(now.valueOf() - (1000 * 60 * 60 * 24))),
					from: this.handlers.dateToTimestamp(new Date(now.valueOf() - (1000 * 60 * 60 * 24 * (days + 1))))
				}
			},
			dialog: {
				open: () => {
					this.setState({
						dialogIsOpen: true
					});
				},
				close: () => {
					this.setState({
						dialogIsOpen: false
					});
				}
			},
			onSearchBrands: async value => {
				let response = await request({
					url: "brands/search",
					data: {
						"name": value
					},
				})
				if (response.status === 200) {
					this.setState({
						foundBrands: response.data.map(item => {
							return {
								label: item.name,
								value: item.alias,
							}
						}),
					})
				}
			},
			getBrandInfo: async ({days, page, perPage}) => {
				let dateRange = this.handlers.getDateRange(days)

				this.setState({
					mainTableIsLoading: true
				}, async () => {
					let response = await request({
						url: "seo/brands",
						data: {
							//"brand_ids": [166, 748, 695, 561, 2321, 20410],
							"date_from": dateRange.from,
							"date_to": dateRange.to,
							"page": page,
							"per_page": this.state.mainTablePerPage
						},
					})
					if (response.status === 200) {
						this.setState({
							mainTableData: response.data.items.map(item => {
								return {
									key: item.id,
									image: {
										src: item.image,
										brandAlias: item.alias
									},
									brand: {
										name: item.name,
										alias: item.alias
									},
									incoming_orders: item.sales || 0,
									dynamic: item.sales_graph ? item.sales_graph.map(item => ({
										name: this.handlers.humanizeDateString(item.name),
										value: item.y
									})) : [],
									average_check: item.average_cost || 0,
									articules: item.products || 0,
									goods_in_stock: item.stocks || 0,
									categories: item.in_categories || 0,
								}
							}),
							mainTablePage: page,
							mainTableRawData: response.data.items,
							mainTableIsLoading: false,
							mainTableTotal: response.data.total,
						})
					}
				});
			},
			humanizeDateString: str => {
				let months = ["ЯНВ", "ФЕВ", "МАРТА", "АПР", "МАЯ", "ИЮНЯ", "ИЮЛЯ", "АВГ", "СЕН", "ОКТ", "НОЯ", "ДЕК"]
				let divided = str.split("-")
				return `${Number(divided[2])} ${months[Number(divided[1]) - 1]}`
			},
			mainTableDaysOnChange: value => {
				this.setState({
					mainTableDays: value,
					salesGraphDays: value,
				}, () => {
					this.handlers.getBrandInfo({
						days: value,
						page: this.state.mainTablePage,
						//perPage: 
					})
				})
			},
			salesGraphDaysOnChange: (value, alias) => {
				this.setState({
					salesGraphDays: value,
				}, () => {
					this.handlers.getSalesGraph({
						alias: alias,
						days: value,
					})
				})
			},
			getSalesGraph: async ({alias, days}) => {
				let dateRange = this.handlers.getDateRange(days)

				this.setState({
					salesGraphIsLoading: true
				}, async () => {
					let response = await request({
						url: `seo/brands/${alias}/sales/graph`,
						data: {
							"date_from": dateRange.from,
							"date_to": dateRange.to,
						},
					})
					if (response.status === 200) {
						this.setState({
							salesGraphData: response.data.map(item => ({
								name: this.handlers.humanizeDateString(item.name),
								value: item.y
							})),
							salesGraphIsLoading: false
						})
					}
				});
			},
			categoriesDaysOnChange: (value, alias) => {
				this.setState({
					categoriesTableDays: value,
				}, () => {
					this.handlers.getCategories({
						alias: alias,
						days: value,
					})
				})
			},
			getCategories: async ({alias, days}) => {
				let dateRange = this.handlers.getDateRange(days)

				this.setState({
					categoriesTableIsLoading: true
				}, async () => {
					let response = await request({
						url: `seo/brands/${alias}/categories`,
						data: {
							"date_from": dateRange.from,
							"date_to": dateRange.to,
							"parent_id": null,
						},
					})
					if (response.status === 200) {
						this.setState({
							categoriesTableData: response.data.map(item => {
								let result = {
									key: item.id,
									category: item.name,
									incoming_orders: item.sales_sum || 0,
									dynamic: item.sales_graph ? item.sales_graph.map(item => ({
										name: item.name,
										value: item.y
									})) : [],
									share: item.sales_percent || 0,
									orders: item.sales || 0,
									ransom: item.buyout || 0,
									discount_price: item.average_cost || 0,
								}
								if (item.children > 0) {
									result.children = [{
										key: "loading",
										category: "Загрузка...",
										incoming_orders: null,
										dynamic: null,
										share: null,
										orders: null,
										ransom: null,
										discount_price: null,
									}]
								}
								return result;
							}),
							categoriesTableRawData: response.data,
							categoriesTableIsLoading: false,
						})
					}
				});
			},
			productsTableDaysOnChange: (value, alias) => {
				this.setState({
					productsTableDays: value,
				}, () => {
					this.handlers.getProductsInfo({
						days: value,
						page: this.state.productsTablePage,
						alias: alias,
						perPage: this.state.productsTablePerPage
					})
				})
			},
			getProductsInfo: async ({days, page, alias, perPage}) => {
				let dateRange = this.handlers.getDateRange(days)

				this.setState({
					productsTableIsLoading: true
				}, async () => {
					let response = await request({
						url: `seo/brands/${alias}/products`,
						data: {
							"date_from": dateRange.from,
							"date_to": dateRange.to,
							"page": page,
							"per_page": perPage
						},
					})
					if (response.status === 200) {
						this.setState({
							productsTableData: response.data.items.map(item => {
								return {
									key: item.id,
									image: item.image,
									product: item.name,
									articule: item.id,
									color_group: item.color,
									discount_price: item.price || 0,
									discount: item.discount || 0,
									incoming_orders: item.sales_sum || 0,
									dynamic: item.sales_graph ? item.sales_graph.map(item => ({
										name: item.name,
										value: item.y
									})) : [],
									orders: item.sales || 0,
								}
							}),
							productsTableRawData: response.data.items,
							productsTableIsLoading: false,
							productsTableTotal: response.data.total,
							productsTablePage: page,
						})
					}
				});
			},
			dateToTimestamp: date => {
				const zeroFirst = s => {
					return `0${s}`.substr(-2)
				}
				return `${date.getFullYear()}-${zeroFirst(date.getMonth() + 1)}-${zeroFirst(date.getDate())}`
			},
			randStr: () => `r${(Math.random() * 10).toString(32).replace(".", "")}`,
			getExactBrandInfo: async ({alias, days = 7, page = 1}) => {
				let dateRange = this.handlers.getDateRange(days)

				let response = await request({
					url: `seo/brands/${alias}`,
					data: {
						//"brand_ids": [id],
						"date_from": dateRange.from,
						"date_to": dateRange.to,
						"page": page,
					},
				})
				if (response.status === 200 && response.data) {
					this.setState({
						currentBrand: response.data
					})
				}
			},
			getCategoriesChildren: async ({brandAlias, parentId, days}) => {
				let dateRange = this.handlers.getDateRange(days)

				let response = await request({
					url: `seo/brands/${brandAlias}/categories`,
					data: {
						"date_from": dateRange.from,
						"date_to": dateRange.to,
						"parent_id": parentId,
					},
				})
				if (response.status === 200) {
					let categoriesTableData = this.state.categoriesTableData.slice()
					let row = this.handlers.recursiveFind(categoriesTableData, "children", item => item.key === parentId)
					console.log(row, parentId, categoriesTableData)
					if (row) {
						row.children = response.data.map(item => {
							let result = {
								key: item.id,
								category: item.name,
								incoming_orders: item.sales_sum || 0,
								dynamic: item.sales_graph ? item.sales_graph.map(item => ({
									name: item.name,
									value: item.y
								})) : [],
								share: item.sales_percent || 0,
								orders: item.sales || 0,
								ransom: item.buyout || 0,
								discount_price: item.average_cost || 0,
							}
							if (item.children > 0) {
								result.children = [{
									key: "loading",
									category: "Загрузка...",
									incoming_orders: null,
									dynamic: null,
									share: null,
									orders: null,
									ransom: null,
									discount_price: null,
								}]
							}
							return result;
						})
						this.setState({
							categoriesTableData: categoriesTableData
						})
					}
				}
			},
			onResize: () => {
				let width = document.documentElement.offsetWidth
				if (width < 768 && !this.state.isMobile) {
					this.setState({
						isMobile: true
					})
				}
				else if (width >= 768 && this.state.isMobile) {
					this.setState({
						isMobile: false
					})
				}
			},
			pluralize: (n, singular, plural, accusative) => {
				n = Math.abs(n)
				let n10 = n % 10;
				let n100 = n % 100;
				if (n10 === 1 && n100 !== 11) {
					return singular;
				}
				if (
					(2 <= n10 && n10 <= 4) &&
					!(12 <= n100 && n100 <= 14)
				) {
					return plural;
				}
				return accusative;
			},
			recursiveFind: (arr, childrenKey, f) => {
				const iterateChildren = children => {
					for (let i = 0; i < children.length; i++) {
						const itemI = children[i]
						if (f(itemI)) {
							return itemI;
						}
						else if (itemI[childrenKey]) {
							const itres = iterateChildren(itemI[childrenKey])
							if (itres) {
								console.log("itres", itres)
								return itres
							}
						}
					}
					return false
				}
				return iterateChildren(arr)
			}
		};

		this.handlers.onSearchBrandsDebounce = debounce(this.handlers.onSearchBrands, 500)

		this.data = {
			advantages: [
				{
					title: "1. Выбираете бренд",
					icon: require("../../img/other/choose-brand.svg"),
				},
				{
					title: "2. Указываете период",
					icon: require("../../img/other/set-period.svg"),
				},
				{
					title: "3. Получаете статистику",
					icon: require("../../img/other/get-stats.svg"),
				},
			],
			mainTableColumns: [
				{
					title: "Фото",
					dataIndex: "image",
					render: image => <LinkPure to={`/brand/${image.brandAlias}`}><img alt="" src={image.src} className={classes.tableImage} /></LinkPure>
				},
				{
					title: "Бренд",
					dataIndex: "brand",
					render: brand => <LinkPure to={`/brand/${brand.alias}`} className={classes.tableBrandLink}>{brand.name}</LinkPure>,
					sorter: (a, b) => a.brand.name < b.brand.name,
				},
				{
					title: "Входящие заказы",
					dataIndex: "incoming_orders",
					render: text => `${text.toLocaleString()} шт`,
					sorter: (a, b) => a.incoming_orders - b.incoming_orders,
					defaultSortOrder: "descend",
				},
				{
					title: "Динамика",
					dataIndex: "dynamic",
					render: data => {
						let len = data.length
						let isUp = data[len - 2].value < data[len - 1].value
						let color = isUp ? "#22C829" : "#E0274F"
						let colorUv = this.handlers.randStr()

						return <AreaChart
							width={118}
							height={42}
							data={data}
							margin={{ top: 3, right: 0, bottom: 3, left: 0 }}
						>
							<defs>
								<linearGradient id={colorUv} x1="0" y1="0" x2="0" y2="1">
									<stop offset="0%" stopColor={color} stopOpacity={0.3}/>
									<stop offset="100%" stopColor={color} stopOpacity={0}/>
								</linearGradient>
							</defs>
							<Area animationDuration={900} strokeWidth={2} type="monotone" dataKey="value" stroke={color} fillOpacity={1} fill={`url(#${colorUv})`} />
						</AreaChart>
					}
				},
				{
					title: "Средний чек",
					dataIndex: "average_check",
					render: text => `${text.toLocaleString()} руб`,
					sorter: (a, b) => a.average_check - b.average_check,
				},
				{
					title: "Артикулов",
					dataIndex: "articules",
					render: text => `${text.toLocaleString()} шт`,
					sorter: (a, b) => a.articules - b.articules,
				},
				{
					title: "Товаров на складе",
					dataIndex: "goods_in_stock",
					render: text => `${text.toLocaleString()} шт`,
					sorter: (a, b) => a.goods_in_stock - b.goods_in_stock,
				},
				{
					title: "Представлен в категориях",
					dataIndex: "categories",
					render: text => `${text.toLocaleString()} шт`,
					sorter: (a, b) => a.categories - b.categories,
				},
			],
			productsTableColumns: [
				{
					title: "Фото",
					dataIndex: "image",
					render: src => <img alt="" src={src} className={classes.tableImage} />
				},
				{
					title: "Товар",
					dataIndex: "product",
					render: text => text,
					sorter: (a, b) => a.product < b.product,
				},
				{
					title: "Артикул",
					dataIndex: "articule",
				},
				{
					title: "Группа цветов",
					dataIndex: "color_group",
					sorter: (a, b) => a.color_group < b.color_group,
				},
				{
					title: "Цена со скидкой",
					dataIndex: "discount_price",
					render: text => `${text.toLocaleString()} руб`,
					sorter: (a, b) => a.discount_price - b.discount_price,
				},
				{
					title: "Скидка",
					dataIndex: "discount",
					render: text => `${text.toLocaleString()}%`,
					sorter: (a, b) => a.discount - b.discount,
				},
				{
					title: "Входящие заказы",
					dataIndex: "incoming_orders",
					render: text => `${text.toLocaleString()} руб`,
					sorter: (a, b) => a.incoming_orders - b.incoming_orders,
					defaultSortOrder: "descend",
				},
				{
					title: "Динамика",
					dataIndex: "dynamic",
					render: data => {
						let len = data.length
						let isUp = len >= 2 ? data[len - 2].value < data[len - 1].value : true
						let color = isUp ? "#22C829" : "#E0274F"
						let colorUv = this.handlers.randStr()

						return <AreaChart
							width={118}
							height={42}
							data={data}
							margin={{ top: 3, right: 0, bottom: 3, left: 0 }}
						>
							<defs>
								<linearGradient id={colorUv} x1="0" y1="0" x2="0" y2="1">
									<stop offset="0%" stopColor={color} stopOpacity={0.3}/>
									<stop offset="100%" stopColor={color} stopOpacity={0}/>
								</linearGradient>
							</defs>
							<Area animationDuration={900} strokeWidth={2} type="monotone" dataKey="value" stroke={color} fillOpacity={1} fill={`url(#${colorUv})`} />
						</AreaChart>
					}
				},
				{
					title: "Заказано товаров",
					dataIndex: "orders",
					render: text => `${text.toLocaleString()} шт`,
					sorter: (a, b) => a.orders - b.orders,
				},
			],
			categoriesTableColumns: [
				{
					title: "Категория",
					dataIndex: "category",
					render: text => text,
					sorter: (a, b) => a.category < b.category,
				},
				{
					title: "Входящие заказы",
					dataIndex: "incoming_orders",
					render: text => text !== null ? `${text.toLocaleString()} руб` : null,
					sorter: (a, b) => a.incoming_orders - b.incoming_orders,
				},
				{
					title: "Динамика",
					dataIndex: "dynamic",
					render: data => {
						if (!data) return null;
						let len = data.length
						let isUp = data[len - 2].value < data[len - 1].value
						let color = isUp ? "#22C829" : "#E0274F"
						let colorUv = this.handlers.randStr()

						return <AreaChart
							width={118}
							height={42}
							data={data}
							margin={{ top: 3, right: 0, bottom: 3, left: 0 }}
						>
							<defs>
								<linearGradient id={colorUv} x1="0" y1="0" x2="0" y2="1">
									<stop offset="0%" stopColor={color} stopOpacity={0.3}/>
									<stop offset="100%" stopColor={color} stopOpacity={0}/>
								</linearGradient>
							</defs>
							<Area animationDuration={900} strokeWidth={2} type="monotone" dataKey="value" stroke={color} fillOpacity={1} fill={`url(#${colorUv})`} />
						</AreaChart>
					}
				},
				{
					title: "Доля от всех заказов бренда",
					dataIndex: "share",
					render: text => text !== null ? `${text.toLocaleString()}%` : null,
					sorter: (a, b) => a.share - b.share,
				},
				{
					title: "Количество заказанных товаров",
					dataIndex: "orders",
					render: text => text !== null ? `${text.toLocaleString()} шт` : null,
					sorter: (a, b) => a.orders - b.orders,
					defaultSortOrder: "descend",
				},
				{
					title: "Выкуп",
					dataIndex: "ransom",
					render: text => text !== null ? `${text.toLocaleString()}%` : null,
					sorter: (a, b) => a.ransom - b.ransom,
				},
				{
					title: "Средняя цена со скидкой",
					dataIndex: "discount_price",
					render: text => text !== null ? `${text.toLocaleString()} руб` : null,
					sorter: (a, b) => a.discount_price - b.discount_price,
				},
			]
		};
	}

	componentDidMount() {
		this.handlers.getBrandInfo({
			days: this.state.mainTableDays,
			page: 1,
			//perPage: this.state.mainTablePerPage
		})
		this.handlers.onResize()
		window.addEventListener("resize", this.handlers.onResize)
	}

	render() {
		return (
			<Fragment>
				{/*<Helmet>
					<link rel="icon" type="image/png" sizes="32x32" href={favicon32x32} />
					<link rel="icon" type="image/png" sizes="16x16" href={favicon16x16} />
					<link rel="mask-icon" href={safariPinnedTab} color="#dc1f52" />
					<meta name="theme-color" content="#1D0249" />
				</Helmet>*/}

				<Dialog
					open={this.state.dialogIsOpen}
					onClose={this.handlers.dialog.close}
					classes={{
						paper: classes.dialog
					}}
					TransitionComponent={Slide}
				>
					<DialogTitle disableTypography classes={{
						root: classes.dialogTitle
					}}>О нас</DialogTitle>
					<img alt="close dialog" className={classes.dialogClose} onClick={this.handlers.dialog.close} src={require("../../img/other/cross.svg")} />
					<DialogContent className={classes.dialogContent}>
						Наш онлайн-сервис синхронизируется с телеграм-ботом, телефоном и почтовым сервисом. Данные собираются ежедневно. Вы можете посмотреть статистику по любому бренду за любой период до 90 дней. <b>Это удобно и бесплатно.</b>
					</DialogContent>
				</Dialog>

				<Container>
					<Header opendDialog={this.handlers.dialog} />
				</Container>



						<FragmentLifecycle
							didMount={() => {
								document.documentElement.scrollTop = 0;
								this.setState({
									currentBrand: null,
									foundBrands: [],

									salesGraphIsLoading: false,
									salesGraphData: [],
									salesGraphDays: 7,

									categoriesTableData: [],
									categoriesTableRawData: [],
									categoriesTablePerPage: 3,
									categoriesTableDays: 7,
									categoriesTableIsLoading: true,

									productsTableData: [],
									productsTableRawData: [],
									productsTablePerPage: 4,
									productsTableDays: 7,
									productsTableIsLoading: true,
									productsTableTotal: 0,
									productsTablePage: 1
								})
							}}
						>
							<Helmet>
								<title>Wildbox</title>
								<meta name="description" content="Бесплатная аналитика брендов Wildberries" />
							</Helmet>

							<Container>
								<div className={classes.tableWrapper}>
									<div className={classes.tableTitle}>Бесплатная аналитика брендов Wildberries</div>
									<Divider style={{margin: "20px 0"}} />
									<Row>
										<Col xs={12} sm={12} md={8} className={classes.inputCol}>
											<AutoComplete
												size="large"
												allowClear
												placeholder="Поиск по названию бренда"
												prefix={
													<img alt="" src={require("../../img/other/search.svg")}
														className={classes.inputIcon}
													/>
												}
												className={mixins.w100}
												onSearch={this.handlers.onSearchBrandsDebounce}
												onSelect={value => {
													this.props.history.push(`/brand/${value}`)
												}}
												options={this.state.foundBrands}
											/>
										</Col>
										<Col xs={12} sm={12} md={4}>
											<Select
												size="large"
												value={this.state.mainTableDays}
												onChange={this.handlers.mainTableDaysOnChange}
												dropdownMatchSelectWidth={false}
												style={{width: "100%"}}
											>
												<Option value={7}>За последние 7 дней</Option>
												<Option value={14}>За последние 14 дней</Option>
												<Option value={30}>За последние 30 дней</Option>
											</Select>
										</Col>
									</Row>
									<Divider style={{margin: "20px 0"}} />
									<Table
										scroll={{x:"100%"}}
										columns={this.data.mainTableColumns}
										dataSource={this.state.mainTableData}
										loading={this.state.mainTableIsLoading}
										pagination={{
											onChange: (page, size) => {
												this.handlers.getBrandInfo({
													days: this.state.mainTableDays,
													page: page,
												})
											},
											//showTotal: total => `Всего: ${total}`,
											total: this.state.mainTableTotal,
											current: this.state.mainTablePage,
											pageSize: this.state.mainTablePerPage,
											showSizeChanger: false,
											size: this.state.isMobile ? "small" : "default"
										}}
										locale={{
											sortTitle: 'Сортировать',
											expand: 'Развернуть',
											collapse: 'Свернуть',
											triggerDesc: 'Нажмите для сортировки по убыванию',
											triggerAsc: 'Нажмите для сортировки по возрастанию',
											cancelSort: 'Нажмите для отмены сортировки',
										}}
									/>
								</div>
							</Container>

							<Container>
								<h1 className={classnames(classes.title, classes.titleFreeAnal)}>Бесплатная аналитика брендов Wildberries всегда под рукой</h1>
								<h5 className={classes.analSubheader}>Просто установите наш телеграм-бот и следите за любыми конкурентами!</h5>
							
								<Row className={classes.advantagesWrapper}>
									{this.data.advantages.map(item => (
										<Col key={item.title} className={classes.advantage}>
											<div className={classes.advantageIconWrapper}>
												<img alt="" src={item.icon} />
											</div>
											<div className={classes.advantageTitle}>{item.title}</div>
										</Col>
									))}
								</Row>
							</Container>
							
							<Container>
								<Row>
									<Col>
										<div className={classes.botWrapper}>
											<div className={classes.botLeft}>
												{/*<img className={classes.iphone} alt="iphone screenshot" src={require("../../img/other/iphone.png")} />
												<img className={classes.iphoneShadow} alt="" src={require("../../img/other/shadow.png")} />*/}
											</div>
											<div className={classes.botRight}>
												<div className={classes.title}>Следите за конкурентами в нашем telegram-боте</div>
												<div className={classes.description}>Наш онлайн-сервис синхронизируются с телеграм-ботом, телефоном и почтовым сервисом. Данные собираются ежедневно. Вы можете посмотреть статистику по любому бренду за любой период до 90 дней. Это удобно и бесплатно.</div>
												<button className={classes.tgButton}>
													<div>Установить telegram-бот</div>
													<img alt="" src={require("../../img/other/telegram.svg")} />
												</button>
												<img alt="" className={classes.arrow} src={require("../../img/other/arrow.svg")} />
											</div>
										</div>
									</Col>
								</Row>
							</Container>
						</FragmentLifecycle>


				<Footer />
			</Fragment>
		);
	}
}

export default App;

//export default withRouter(App);