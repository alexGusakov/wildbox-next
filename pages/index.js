import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
//import { StylesProvider, MuiThemeProvider, createMuiTheme, createGenerateClassName } from "@material-ui/core/styles";
import App from './components/App';
//import * as serviceWorker from './serviceWorker';
import './css/global.scss';
import "./fonts/SanFrancisco/stylesheet.css";

const Apps = () => (
  <div>
    <App />
  </div>
);
export default Apps;

/*
ReactDOM.hydrate(
	<React.StrictMode>
		<Router>
			<App />
		</Router>
	</React.StrictMode>,
	document.getElementById("root")
);
*/

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();